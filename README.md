copy to have a basic CRUD app with Fastapi, which comes with a SQLite db

to run:
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

uvicorn main:app --reload


---
Check out http://localhost:8000/docs for the OpenAPI specs